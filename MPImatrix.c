#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>    // for execution time

#define N 1000
#define DIV_NUM 5
#define MASTER 0               /* taskid of first task */
#define FROM_MASTER 1          /* setting a message type */
#define FROM_WORKER 2          /* setting a message type */

int main (int argc, char *argv[])
{
  int operation;           /* operation: 0-sum, 1-sub, 2-mult, 3-div */
  int	numtasks,            /* number of tasks in partition */
  	taskid,                /* a task identifier */ //RANK
  	numworkers,            /* number of worker tasks */
  	source,                /* task id of message source */
  	dest,                  /* task id of message destination */
  	mtype,                 /* message type */
  	rows,                  /* rows of matrix A sent to each worker */
  	averow, extra, offset, /* used to determine rows sent to each worker */
  	i, j, k, rc;           /* misc */

  static double a[N][N];
  static double b[N][N];
  static double c[N][N];

  MPI_Status status;

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD,&numtasks);
  MPI_Comm_rank(MPI_COMM_WORLD,&taskid);

  if (numtasks < 2 ) 
  {
    printf("Need at least two MPI tasks. Quitting...\n");
    MPI_Abort(MPI_COMM_WORLD, rc);
    exit(1);
  }

  numworkers = numtasks-1;
  operation = atoi(argv[2]);

/**************************** master task ************************************/
   if (taskid == MASTER)
   {
      printf("-------------------------------------\n");
      printf("------------MPI OPERATION------------\n");
      printf("-------------------------------------\n");
      printf("Operation: %d\n", operation);
      printf("Number of tasks: %d\n", numtasks);
      printf("Initializing arrays...\n");
      for (i=0; i<N; i++)
         for (j=0; j<N; j++)
         {
            a[i][j]= i+j;
            b[i][j]= i*j;
         }

      printf("Calculating operation ...\n");
	    clock_t begin = clock();
      /* Send matrix data to the worker tasks */
      averow = N/numworkers;
      extra = N%numworkers;
      offset = 0;
      mtype = FROM_MASTER;
      for (dest=1; dest<=numworkers; dest++)
      {
         rows = (dest <= extra) ? averow+1 : averow;
         MPI_Send(&offset, 1, MPI_INT, dest, mtype, MPI_COMM_WORLD);
         MPI_Send(&rows, 1, MPI_INT, dest, mtype, MPI_COMM_WORLD);
         MPI_Send(&a[offset][0], rows*N, MPI_DOUBLE, dest, mtype,
                   MPI_COMM_WORLD);
         MPI_Send(&b, N*N, MPI_DOUBLE, dest, mtype, MPI_COMM_WORLD);
         offset = offset + rows;
      }
      /* Receive results from worker tasks */
      mtype = FROM_WORKER;
      for (i=1; i<=numworkers; i++)
      {
         source = i;
         MPI_Recv(&offset, 1, MPI_INT, source, mtype, MPI_COMM_WORLD, &status);
         MPI_Recv(&rows, 1, MPI_INT, source, mtype, MPI_COMM_WORLD, &status);
         MPI_Recv(&c[offset][0], rows*N, MPI_DOUBLE, source, mtype,
                  MPI_COMM_WORLD, &status);
      }

			clock_t end = clock();
	    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	    printf("Execution time: %0.15f [s]\n",time_spent);
      printf("-------------------------------------\n");
   }

/**************************** worker task ************************************/
   if (taskid > MASTER)
   {
      mtype = FROM_MASTER;
      MPI_Recv(&offset, 1, MPI_INT, MASTER, mtype, MPI_COMM_WORLD, &status);
      MPI_Recv(&rows, 1, MPI_INT, MASTER, mtype, MPI_COMM_WORLD, &status);
      MPI_Recv(&a, rows*N, MPI_DOUBLE, MASTER, mtype, MPI_COMM_WORLD, &status);
      MPI_Recv(&b, N*N, MPI_DOUBLE, MASTER, mtype, MPI_COMM_WORLD, &status);
      switch (operation) {
        case 0: /* Same size */
          for (i=0; i<rows; i++){
            for (j = 0; j < N; j++) {
              c[i][j] = a[i][j] + b[i][j];
            }
          }
          break;
        case 1: /* Same size */
          for (i=0; i<rows; i++){
            for (j = 0; j < N; j++) {
              c[i][j] = a[i][j] - b[i][j];
            }
          }
          break;
        case 2:
          for (k=0; k<N; k++)
            for (i=0; i<rows; i++)
            {
              c[i][k] = 0.0;
                for (j=0; j<N; j++)
                  c[i][k] = c[i][k] + a[i][j] * b[j][k];
          }
          break;
				case 3:
				for (i=0; i<rows; i++){
					for (j = 0; j < N; j++) {
						c[i][j] = a[i][j]/DIV_NUM;
					}
				}
				break;
      }

      mtype = FROM_WORKER;
      MPI_Send(&offset, 1, MPI_INT, MASTER, mtype, MPI_COMM_WORLD);
      MPI_Send(&rows, 1, MPI_INT, MASTER, mtype, MPI_COMM_WORLD);
      MPI_Send(&c, rows*N, MPI_DOUBLE, MASTER, mtype, MPI_COMM_WORLD);
   }
   MPI_Finalize();
}
