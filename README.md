# mpi_estructuras_2

Matrix calculator (addition, subtraction, multiplication and division) using MPI

## Dependencies
Make sure MPI is install:
```
  sudo apt install mpich
```
## How to build and run the project
1. Compile
```
  make
```
2. Execute with make
```
  make run
```
If you want to change the input parameters like the operation and number of threads, read the next step.
3. Execute manual
```
mpiexec -n <threads> <exe> -op <operation>
```
The input parameters are as follows:
* Number of threads (-n)
* Operation (-op)

Where:
 - 0: addition
 - 1: substraction
 - 2: multiplication
 - 3: division
