all: simple mpi
.PHONY: all

simple: simpleMatrix.c
	gcc -o simple simpleMatrix.c

mpi: MPImatrix.c
	mpicc MPImatrix.c -o mpi

run:
	./simple -op 0
	mpiexec -n 16 ./mpi -op 0

clean:
	rm -f ./simple ./mpi