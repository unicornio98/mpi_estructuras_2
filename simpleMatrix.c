#include <stdio.h>
#include <stdlib.h>
#include <time.h>    // for execution time

#define N 1000
#define DIV_NUM 5

enum operation
{
  ADD,
  SUB,
  MUL,
  DIV,
};

void print_helper()
{
  printf("-------------------------------------\n");
  printf("Operations:\n");
  printf("\t0: ADDITION\n");
  printf("\t1: SUBTRACTION\n");
  printf("\t2: MULTIPLICATION\n");
  printf("\t3: DIVISION\n");
}

int main (int argc, char *argv[])
{
  print_helper();

  int operation = atoi(argv[2]);;

  /* Arrays A & B */
  static double a[N][N];
  static double b[N][N];
  static double c[N][N];
  printf("-------------------------------------\n");
  printf("-----------SIMPLE OPERATION----------\n");
  printf("-------------------------------------\n");
  printf("Operation: %d\n", operation);
  printf("Initializing arrays...\n");
  /* Fill matrix A & B */
  for (int i=0; i<N; i++)
     for (int j=0; j<N; j++)
     { 
        a[i][j]= i+j;
        b[i][j]= i*j;
     }

  /* Time begins */
  printf("Calculating operation...\n");
  clock_t begin = clock();

  /* Choose operation */
  switch (operation) {
    case ADD: /* Same size */
      for (int i=0; i < N; i++){
        for (int j = 0; j < N; j++) {
          c[i][j] = a[i][j] + b[i][j];
        }
      }
      break;
    case SUB: /* Same size */
      for (int i=0; i < N; i++){
        for (int j = 0; j < N; j++) {
          c[i][j] = a[i][j] - b[i][j];
        }
      }
      break;
    case MUL:
      for (int k=0; k<N; k++)
        for (int i=0; i<N; i++)
        {
          c[i][k] = 0.0;
          for (int j=0; j<N; j++)
            c[i][k] = c[i][k] + a[i][j] * b[j][k];
      }
      break;
		case DIV:
		for (int i=0; i < N; i++){
			for (int j = 0; j < N; j++) {
				c[i][j] = a[i][j]/DIV_NUM;
			}
		}
		break;
  }
  clock_t end = clock();
  double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  printf("Execution time: %0.15f [s]\n",time_spent);
  printf("-------------------------------------\n");
  }
